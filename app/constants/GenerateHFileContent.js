/**
 * genereate the string of .h model for sapo
 * @param fileName: name of the saved file
 */
export const generateHFileContent = fileName => {
  let content = "#ifndef " + fileName.toUpperCase() + "_H_ \n";
  content += "#define " + fileName.toUpperCase() + "_H_ \n\n";

  //include libraries
  content += `#include "Always.h"
#include "Eventually.h"
#include "Until.h"
#include "Conjunction.h"
#include "Disjunction.h"
#include "STL.h"
#include "Atom.h"
#include "Model.h"
\n\n`;

  content +=
    "class " +
    fileName +
    " : public Model { \n\n private: \n\n public: \n" +
    fileName +
    "(); \n\n }; \n #endif";

  return content;
};
