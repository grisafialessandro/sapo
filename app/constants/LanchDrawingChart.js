const { exec, execSync, execFileSync } = require("child_process");
const fs = require("fs");

// launch Octave or Matlab for drawing the plots
// require the array of variables
// .m files are saved in sapoCore/bin
export const lanchDrawingChart = (
  printSettings,
  fileName,
  variables,
  parameters
) => {
  // if the user want to draw
  if (printSettings.drawingSw !== "none") {
    // clean the obj
    let NotEmptyCharts = printSettings.charts.filter(item => {
      return item.x_axis !== "" && item.y_axis !== ""; // allow z="" for 2D graph
    });

    // extract names
    let cleanVariables = variables.map(item => {
      return item.name;
    });

    let cleanParameters = parameters.map(item => {
      return item.name;
    });

    // modify the file because the API of plotregion that needs a 3*3 matrix
    // load the files
    NotEmptyCharts.forEach((item, index) => {
      let currentPathFileName = "sapoCore/bin/" + fileName + index + ".m";
      let data = fs.readFileSync(currentPathFileName).toString();

      let chartType;

      // understend if the chart is for variables or parameters, look for an axis label
      for (let i = 0; i < cleanVariables.length; i++) {
        if (data.indexOf("('" + cleanVariables[i] + "')") !== -1) {
          // look for a variable
          chartType = "variables";
          break;
        }
      }
      let completeString; // will contain the string that will be inserted in file.m

      if (chartType === "variables") {
        // create code to sostitute
        let nVar = cleanVariables.length; // of all the variable of the system

        // generate the indexes for the variables to draw
        let variableListToPrint = [];

        // item is an element of charts that is an array
        // index in matlab starts from 1
        variableListToPrint.push(1 + cleanVariables.indexOf(item.x_axis));
        variableListToPrint.push(1 + cleanVariables.indexOf(item.y_axis));
        if (item.z_axis !== "") {
          variableListToPrint.push(1 + cleanVariables.indexOf(item.z_axis));
        }

        // if the selected variable in not avaiable anymore -> a element in variableListToPrint is 0 because of the +1
        if (variableListToPrint.indexOf(0) !== -1) {
          alert("Error in printing");
          return;
        }

        // variablesForRow needs to contains all the indexes and their idex+nVar
        let variablesForRow = variableListToPrint.map(item => {
          return nVar + item;
        });

        variablesForRow = variablesForRow.concat(variableListToPrint);
        variablesForRow.sort((a, b) => a - b); // numerical sorting

        let stringForRow = "[" + variablesForRow.join(" ") + "]"; // toString without commas
        let stringForColumn = "[" + variableListToPrint.join(" ") + "]";

        //plotregion( -Ab([2 4 5 7 9 10], [4 2 5]),-Ab([2 4 5 7 9 10], 6), [], [], 'w' );
        completeString =
          "plotregion( -Ab(" +
          stringForRow +
          ", " +
          stringForColumn +
          "), -Ab(" +
          stringForRow +
          ", " +
          (nVar + 1) +
          "), [], [], 'w',0 );";
      } else {
        // case for parameters
        let nPar = cleanParameters.length; // of all the variable of the system

        // generate the indexes for the variables to draw
        let parameterListToPrint = [];

        // item is an element of charts that is an array
        // index in matlab starts from 1
        parameterListToPrint.push(1 + cleanParameters.indexOf(item.x_axis));
        parameterListToPrint.push(1 + cleanParameters.indexOf(item.y_axis));
        if (item.z_axis !== "") {
          parameterListToPrint.push(1 + cleanParameters.indexOf(item.z_axis));
        }

        if (parameterListToPrint.indexOf(0) !== -1) {
          alert("Error in printing");
          return;
        }

        // parametersForRow needs to contains all the indexes and their idex+nVar
        let parametersForRow = parameterListToPrint.map(item => {
          return nPar + item;
        });

        parametersForRow = parametersForRow.concat(parameterListToPrint);
        parametersForRow.sort((a, b) => a - b); // numerical sorting

        let stringForRow = "[" + parametersForRow.join(" ") + "]"; // toString without commas
        let stringForColumn = "[" + parameterListToPrint.join(" ") + "]";

        completeString =
          "plotregion( -Ab(" +
          stringForRow +
          ", " +
          stringForColumn +
          "), -Ab(" +
          stringForRow +
          ", " +
          (nPar + 1) +
          "), [], [], 'w',0 );";
      }

      // modify the files
      let results = data.replace(/plotregion.+\;/g, completeString);
      //console.log(results);
      // write (overwrite)
      fs.writeFileSync(currentPathFileName, results);
    });

    // launching section
    let commandToLaunch = printSettings.drawingSw;
    // possible additional parameter, now not needed
    if (commandToLaunch === "octave") {
      commandToLaunch += "";
    }

    NotEmptyCharts.forEach((item, index) => {
      // aync because of multiple charts
      exec(
        "cd sapoCore/bin/ && " +
          commandToLaunch +
          " " +
          fileName +
          index +
          ".m" +
          " &",
        (err, stdout, stderr) => {
          if (err) {
            console.log(err);
          }
          console.log(`chart stdout: ${stdout}`);
          console.log(`chart stderr: ${stderr}`);
        }
      );
    });
  }
};

const generateStringForVariables = stringDataFile => {};
