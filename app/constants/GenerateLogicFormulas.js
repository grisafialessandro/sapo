const { execSync, execFileSync } = require("child_process");
/*
 * given in input the array of string, check for syntatical errors and
 * return the C code for the input formulas, if there is an error returns undefined
 */
export const generateLogic = LogicFormulas => {
  // ignoring empty formulas
  let NotEmptyLogicFormulas = LogicFormulas.filter(item => {
    return item !== "";
  });

  try {
    // make parser, always executed (even when there is already the compiled parser)
    // execSync pass the stderr in nodejs
    // default maxBuffer is 1MB depending of nodejs version. see documentation
    let compilation = execSync("cd parser && make");
    console.log(compilation.toString());

    // union of the formulas
    let allFormulas = NotEmptyLogicFormulas[0];
    for (let i = 1; i < NotEmptyLogicFormulas.length; i++) {
      allFormulas = allFormulas + "∧" + NotEmptyLogicFormulas[i];
    }
    allFormulas += "\n"; // needed by parser
    console.log(allFormulas);
    // call the parser that will return the C++ code for the formulas
    let results = execFileSync("./LogicParser.out", {
      input: allFormulas,
      cwd: "./parser"
    });

    console.log(results.toString());
    return results; // string containing C code for the model
  } catch (err) {
    console.log("printing error log");
    console.log("standard output: " + err.stdout.toString());
    console.log("standard error: " + err.stderr.toString());
    console.log("pid: " + err.pid);
    console.log("signal: " + err.signal);
    console.log("error status code: " + err.status);
    console.log("end error log");

    return undefined;
  }
};
