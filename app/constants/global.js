const { exec, execSync, execFileSync } = require("child_process");
const fs = require("fs"); // Load the File System to execute our common tasks (CRUD)
const { dialog } = require("electron").remote;

// it's assumed that the default pwd is sapo, even when node.js doc says it's null
let childProcess;

export const colors = {
  red: "#C62828",
  green: "#558B2F",
  orange: "#FF8F00"
};

export const deepCopy = value => {
  return JSON.parse(JSON.stringify(value));
};

/**
 * @param string: string to capitalize
 */
export const capitalizeFirstLetter = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

/**
 * Function to create a file in a specified path with a specified content
 * @param path: path where the file needs to be created
 * @param content: content of the file that will be created
 */
export const createFile = (path, content) => {
  fs.writeFile(path, content, err => {
    if (err) {
      alert("An error ocurred creating the file " + err.message);
      return;
    }
  });
};

/**
 * @param command: shell command to execute (to concatenate commands use && notation)
 * @param callback: function called after the execution of the shell comand to reset state in the main file
 */
export const executeShellCommand = (
  command,
  callbackOpenGraph,
  callbackKill
) => {
  console.log("exec " + command);
  childProcess = exec(
    command,
    { maxBuffer: 10 * 1024 * 1024 },
    (err, stdout, stderr) => {
      if (err) {
        // node couldn't execute the command
        console.log(err);
        return;
      }

      // ! used also by other. not oly variables
      let index = stdout.indexOf("Variables values");
      console.log(index);
      let outputString = stdout.substr(index);

      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);

      dialog.showSaveDialog(
        {
          filters: [
            {
              name: "txt",
              extensions: ["txt"]
            }
          ],
          title: "Save the result file"
        },
        fileName => {
          if (fileName === undefined) {
            console.log("You didn't save the file");
            return;
          }

          // fileName is a string that contains the path and filename created in the save file dialog.
          fs.writeFile(fileName, outputString, async err => {
            if (err) {
              alert("An error ocurred creating the file " + err.message);
              return;
            }

            alert("The output file has been succesfully saved");

            await callbackOpenGraph();
            callbackKill();
          });
        }
      );
    }
  );
};

export const killShellCommand = () => {
  if (childProcess != undefined) {
    childProcess.kill();
  } else {
    alert("No process is running");
  }
};
