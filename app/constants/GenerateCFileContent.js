import { generateLogic } from "./GenerateLogicFormulas.js";

/**
 * generate the C model of the system
 * NOT COMPLETELY IMPLEMENTED
 * @param fileName: name of the saved file
 * @param variables: JSON array containing all of the variables and respectives domain
 * @param parameters: JSON array containing all of the parameters and respectives domain
 * @param equations: JSON array containing all of the equations
 * @param {boolean} boxesMethod: used to identify if the approximation method used is boxes
 * @param parametersMatrix: matrix associated at the parameters, that can be boxes or polytopes
 * @param lMatrix: matrix of the directions (can be custom in parallelotopes and polytopes method)
 * @param tMatrix: T matrix (can be custom in polytopes method)
 * @param logicFormulas: array of string containing the STLs
 * @param {boolean} reachability: used to identify if it's a reachability analysis
 * @param {boolean} synthesis: used to identify if it's a synthesis computation
 * @param {boolean} polytopesMethod: used to identify if the approximation method used is parallelotopes
 * @param {boolean} parallelotopesMethod: used to identify if the approximation method used is polytopes
 * @returns {string}: string of C code that represent the model
 */
export const generateCFileContent = (
  fileName,
  variables,
  parameters,
  equations,
  reachability,
  synthesis,
  boxesMethod,
  polytopesMethod,
  parallelotopesMethod,
  parametersMatrix,
  lMatrix,
  tMatrix,
  logicFormulas,
  printSettings
) => {
  /*
    Order of the generated C model file
    1) code for include and constructor name
    2) declared all variables needed except the various matrix
    3) defined all symbols, and also specified which are variables and which are parameters
    4) defined the system equations
    5) filled the offset vectors
    6) setted the parameters, variables and equations
    7) defined and filled L matrix
    8) defined and filled T matrix
    9) defined and filled pA matrix
    10) initial parameter set
    11) initialization of the system
    12) STL specifications
  */

  //------------------- declaration of working variables ----------------------
  let content = `#include "` + fileName + `.h"`; // this variable will contain all the code

  content += "\n\n " + fileName + "::" + fileName + "(){ \n";

  content +=
    `
/////////// THE Model /////////////
strcpy(this->name,"` +
    fileName +
    `");\nint dim_sys = ` +
    equations.length +
    `; \n`;

  let numberOfVar = variables.filter(element => {
    return !element.lMatrixExtra;
  }).length;
  let numberOfParam = parameters.length;

  // initialization of strings that contains parts of the code
  let staticDeclaration =
    `\nint num_dirs = ` +
    numberOfVar +
    `; 
vector <double> offp(num_dirs, 0);  
vector <double> offm(num_dirs, 0);
vector <double> pb(` +
    numberOfParam * 2 +
    `,0); \n`;

  let symbolString = "symbol ";
  let lstVarsString = "lst vars = {";
  let lstParamsString = "lst params = {";
  let lstEquations = "lst dyns = {";
  let varRangeDeclaration = "";
  let paramRangeDeclaration = "";
  let eqDeclaration = "";

  //------------ C code generation of variables -------------------------------
  variables.map((item, index) => {
    if (!item.lMatrixExtra) {
      symbolString += item.name + '("' + item.name + '")';
      lstVarsString += item.name;
    }

    // offm is the lowerBound of the domain of the variables, offp is upperBound
    varRangeDeclaration += "offp[" + index + "] = " + item.upperBound + ";";
    varRangeDeclaration += " offm[" + index + "] = " + -item.lowerBound + ";"; // -lowerBound!
    varRangeDeclaration += "\n";

    if (!item.lMatrixExtra) {
      // d is for difference (discrete system is made of difference equation)
      let eqName = "d" + item.name; // reppresent the left side of the equation
      // write right side of the equation
      eqDeclaration +=
        "ex " + eqName + " = " + equations[index].equation + "; \n";
      lstEquations += eqName;

      if (index != numberOfVar - 1) {
        symbolString += ", ";
        lstVarsString += ", ";
        lstEquations += ", ";
      }
    }
  });

  // "" is because there is a check later, that control if symbolString is unmodified
  symbolString += numberOfParam > 0 && numberOfVar > 0 ? ", " : "";

  //------------ C code generation of parameters ------------------------------
  let parameterIndex = 0;

  parameters.map((item, index) => {
    symbolString += item.name + '("' + item.name + '")';
    lstParamsString += item.name;

    paramRangeDeclaration +=
      "pb[" + parameterIndex + "] = " + item.upperBound + ";";

    parameterIndex++;

    paramRangeDeclaration +=
      "pb[" + parameterIndex + "] = " + -item.lowerBound + "; \n";

    parameterIndex++;

    if (index != numberOfParam - 1) {
      symbolString += ", ";
      lstParamsString += ", ";
    }
  });

  //various checks: if this variables are untouched put them to empty, else add ';'
  if (symbolString !== "symbol ") {
    symbolString += ";";
  } else {
    symbolString = "";
  }

  if (lstVarsString !== "lst vars = {") {
    lstVarsString += "};";
  } else {
    lstVarsString = "";
  }

  if (lstParamsString !== "lst params = {") {
    lstParamsString += "};";
  } else {
    lstParamsString = "";
  }

  if (lstEquations !== "lst dyns = {") {
    lstEquations += "};";
  } else {
    lstEquations = "";
  }

  // concatenate the various string to content
  content += staticDeclaration + "\n";
  content += symbolString + "\n";
  content += lstVarsString + "\n";
  content += lstParamsString + "\n";
  content += eqDeclaration + "\n";
  content += lstEquations + "\n";
  content += varRangeDeclaration + "\n";
  content += paramRangeDeclaration + "\n";

  //connect scope vars, dyns, params
  if (variables.length > 0) {
    content += "this->vars = vars; \nthis->dyns = dyns; \n";
  }

  if (parameters.length > 0) {
    content += "this->params = params; \n";
  }

  // ---------------------- Matrix only in the boxes method --------------------------
  //to do change condition of if to check which methos is being used to change t and l generation
  if (true) {
    //Generate identy matrix L for boxes method
    content +=
      `
vector<double> Li(dim_sys, 0);
vector< vector<double> > L(` +
      variables.length +
      `,Li); \n`;

    let lMatrixData = lMatrix._data;

    for (var row = 0; row < variables.length; row++) {
      const rowElement = lMatrixData[row];
      for (var column = 0; column < numberOfVar; column++) {
        content +=
          "L[" + row + "][" + column + "] = " + rowElement[column] + ";";
      }
      content += "\n";
    }

    let tMatrixData = tMatrix._data;

    //Generate T matrix for boxes method
    content +=
      `
vector<int> Ti(dim_sys, 0);
vector< vector<int> > T(` +
      tMatrixData.length +
      `,Ti); \n`;

    for (var row = 0; row < tMatrixData.length; row++) {
      const rowElement = tMatrixData[row];
      for (var column = 0; column < numberOfVar; column++) {
        content +=
          "T[" + row + "][" + column + "] = " + rowElement[column] + ";";
      }
      content += "\n";
    }
  }

  //--------------------------------------------------------------------------------

  //Generate paramtere matrix pA
  let matrix = parametersMatrix._data;

  content +=
    `\nvector<double> pAi(` +
    numberOfParam +
    `, 0);\nvector< vector<double> > pA(` +
    matrix.length +
    `,pAi); \n`;

  if (matrix !== undefined) {
    for (let rowIndex = 0; rowIndex < matrix.length; rowIndex++) {
      const rowElement = matrix[rowIndex];
      for (let colIndex = 0; colIndex < rowElement.length - 1; colIndex++) {
        const colElement = rowElement[colIndex];
        content +=
          "pA[" + rowIndex + "][" + colIndex + "] = " + colElement + "; ";
      }
      content += "\n";
    }

    //finish filling pb vector
    for (
      let rowIndex = numberOfParam * 2;
      rowIndex < matrix.length;
      rowIndex++
    ) {
      const rowElement = matrix[rowIndex];
      if (rowElement[rowElement.length - 1] != undefined) {
        content +=
          "pb[" +
          rowIndex +
          "] = " +
          rowElement[rowElement.length - 1] +
          "; \n";
      }
    }
  }

  //SAPO stuff reachability
  content += `
Bundle *B = new Bundle(L,offp,offm,T);
this->reachSet = B;
`;

  if (parameters.length > 0) {
    content += `
LinearSystem *parameters = new LinearSystem(pA,pb);
LinearSystemSet *parameter_set = new LinearSystemSet(parameters);
this->paraSet = parameter_set;\n\n`;
  }

  //synthesis formulas code
  if (synthesis === true) {
    let codeForFormulas = generateLogic(logicFormulas);
    if (codeForFormulas !== undefined) {
      content += codeForFormulas;
    } else {
      // parser faild some alert code
    }
  }

  content += `}`;

  return content;
};
