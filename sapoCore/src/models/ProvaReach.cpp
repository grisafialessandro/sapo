#include "ProvaReach.h"

 ProvaReach::ProvaReach(){ 

/////////// THE Model /////////////
strcpy(this->name,"ProvaReach");
int dim_sys = 2; 

int num_dirs = 2; 
vector <double> offp(num_dirs, 0);  
vector <double> offm(num_dirs, 0);
vector <double> pb(2,0); 

symbol x("x"), y("y"), a("a");
lst vars = {x, y};
lst params = {a};
ex dx = x+1+a; 
ex dy = y+1; 

lst dyns = {dx, dy};
offp[0] = 0; offm[0] = 0;
offp[1] = 0; offm[1] = 0;

pb[0] = 0.2;pb[1] = -0.1; 

this->vars = vars; 
this->dyns = dyns; 
this->params = params; 

vector<double> Li(dim_sys, 0);
vector< vector<double> > L(num_dirs,Li); 
L[0][0] = 1;L[0][1] = 0;
L[1][0] = 0;L[1][1] = 1;

vector<int> Ti(dim_sys, 0);
vector< vector<int> > T(1,Ti); 
T[0][0] = 0; 
T[0][1] = 1; 

vector<double> pAi(1, 0);
vector< vector<double> > pA(2,pAi); 
pA[0][0] = 1; 
pA[1][0] = -1; 

Bundle *B = new Bundle(L,offp,offm,T);
LinearSystem *parameters = new LinearSystem(pA,pb);
LinearSystemSet *parameter_set = new LinearSystemSet(parameters);

this->reachSet = B;
this->paraSet = parameter_set;

}