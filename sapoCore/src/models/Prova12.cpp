#include "Prova12.h"

 Prova12::Prova12(){ 

/////////// THE Model /////////////
strcpy(this->name,"Prova12");
int dim_sys = 2; 

int num_dirs = 2; 
vector <double> offp(num_dirs, 0);  
vector <double> offm(num_dirs, 0);
vector <double> pb(0,0); 

symbol a("a"), b("b");
lst vars = {a, b};

ex da = a+b; 
ex db = a+b; 

lst dyns = {da, db};
offp[0] = 0; offm[0] = 0;
offp[1] = 0; offm[1] = 0;


this->vars = vars; 
this->dyns = dyns; 

vector<double> Li(dim_sys, 0);
vector< vector<double> > L(num_dirs,Li); 
L[0][0] = 1;L[0][1] = 0;
L[1][0] = 0;L[1][1] = 1;

vector<int> Ti(dim_sys, 0);
vector< vector<int> > T(1,Ti); 
T[0][0] = 0; 
T[0][1] = 1; 

vector<double> pAi(0, 0);
vector< vector<double> > pA(0,pAi); 


Bundle *B = new Bundle(L,offp,offm,T);
LinearSystem *parameters = new LinearSystem(pA,pb);
LinearSystemSet *parameter_set = new LinearSystemSet(parameters);

this->reachSet = B;
this->paraSet = parameter_set;

}