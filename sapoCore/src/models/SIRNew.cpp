#include "SIRNew.h"

 SIRNew::SIRNew(){ 

/////////// THE Model /////////////
strcpy(this->name,"SIRNew");
int dim_sys = 3; 

int num_dirs = 3; 
vector <double> offp(num_dirs, 0);  
vector <double> offm(num_dirs, 0);
vector <double> pb(0,0); 

symbol s("s"), i("i"), r("r");
lst vars = {s, i, r};

ex ds = s - (0.34*s*i)*0.1; 
ex di = i + (0.34*s*i - 0.05*i)*0.1; 
ex dr = r + 0.05*i*0.1; 

lst dyns = {ds, di, dr};
offp[0] = 0.8; offm[0] = -0.79;
offp[1] = 0.2; offm[1] = -0.19;
offp[2] = 0.0001; offm[2] = -0.000099;


this->vars = vars; 
this->dyns = dyns; 

vector<double> Li(dim_sys, 0);
vector< vector<double> > L(num_dirs,Li); 
L[0][0] = 1;L[0][1] = 0;L[0][2] = 0;
L[1][0] = 0;L[1][1] = 1;L[1][2] = 0;
L[2][0] = 0;L[2][1] = 0;L[2][2] = 1;

vector<int> Ti(dim_sys, 0);
vector< vector<int> > T(1,Ti); 
T[0][0] = 0; 
T[0][1] = 1; 
T[0][2] = 2; 

vector<double> pAi(0, 0);
vector< vector<double> > pA(0,pAi); 


Bundle *B = new Bundle(L,offp,offm,T);
this->reachSet = B;
}