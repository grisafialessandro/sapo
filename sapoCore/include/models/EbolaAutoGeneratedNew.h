#ifndef EBOLAAUTOGENERATEDNEW_H_ 
#define EBOLAAUTOGENERATEDNEW_H_ 

#include "Always.h"
#include "Eventually.h"
#include "Until.h"
#include "Conjunction.h"
#include "Disjunction.h"
#include "STL.h"
#include "Atom.h"
#include "Model.h"


class EbolaAutoGeneratedNew : public Model { 

 private: 

 public: 
EbolaAutoGeneratedNew(); 

 }; 
 #endif