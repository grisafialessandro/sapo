#ifndef SIRNEW_H_ 
#define SIRNEW_H_ 

#include "Always.h"
#include "Eventually.h"
#include "Until.h"
#include "Conjunction.h"
#include "Disjunction.h"
#include "STL.h"
#include "Atom.h"
#include "Model.h"


class SIRNew : public Model { 

 private: 

 public: 
SIRNew(); 

 }; 
 #endif