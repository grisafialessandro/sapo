#ifndef EBOLACORRECTAUTOGENERATE_H_ 
#define EBOLACORRECTAUTOGENERATE_H_ 

#include "Always.h"
#include "Eventually.h"
#include "Until.h"
#include "Conjunction.h"
#include "Disjunction.h"
#include "STL.h"
#include "Atom.h"
#include "Model.h"


class EbolaCorrectAutoGenerate : public Model { 

 private: 

 public: 
EbolaCorrectAutoGenerate(); 

 }; 
 #endif