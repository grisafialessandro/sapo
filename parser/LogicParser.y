/******************************************************************************
** This file contains the parser definition for the formula (modified STL).  **
** The main is at the bottom.                                                **
** If debug is needed:                                                       **
**     bison -v for printing the file that represent the DFA of the parser   **
**     yydebug = 1; in main for showing the parser computation               **
** The productions of the grammar have a particular form that it had been    ** 
** chosen because it allow the creation of a binary tree.                    **
** The grammar has 3 unresolved shift/reduce conflicts. probably caused by   **
** precedence for _U_ _U_ .                                                  **
** Building a parser in Javascript, with Antlr or Jison, wolud be better for **
** consistency with the project.                                             **
** credits:                                                                  **
**     Waleed Qadir Khan https://github.com/STLInspector/STLInspector        **
**     https://github.com/STLInspector/STLInspector                          **
**                                                                           ** 
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/

%{
	#include"utils/treeUtils.h"
	#include"utils/treePrint.h"
		
	#include<stdio.h>
	#include<stdlib.h> // exit()
	
	#define YYDEBUG 1 // include trace facilities for parser
	extern int yylex();
	extern void yyerror();
%}

// definition of yylval that needs to contains various type of data
%union
{
	char* str;
	struct node* node;
	double fp;
}

%token TK_GLOBALLY TK_FINALLY TK_UNTIL
%token TK_AND TK_OR TK_NOT
%token TK_END TK_VAR TK_NUM_DOUBLE
%token TK_LESS TK_GE

// priority and associativity of the tokens
%left TK_AND TK_OR
%left TK_GE TK_LESS
%left '+' '-'
%left '*' '/'
%left TK_NEG	// for unary minus
%right '^'

%left TK_GLOBALLY TK_FINALLY TK_UNTIL
%left TK_NOT

%type <node> input f sub_f expression intermediate_for_until time_slice power
%type <fp> TK_NUM_DOUBLE
%type <str> TK_VAR

%%
input:f TK_END{ 
		//print2D($1); printf("\n\n"); 
		simplify($1);
		//print2D($1); printf("\n\n");
		//printf("%s\n\n\n", treeToString($1)); 
		printf("%s\n", treeToCode($1));
		return 0; // this command terminates the parsing
		}
		| TK_END{ return 0;}
;

f:	f TK_AND f									{ $<node>$= new_node(AND, $1, $3); }
	|f TK_OR f									{ $<node>$= new_node(OR, $1, $3); }
	|TK_NOT f									{ $<node>$= new_node(NOT, $2, NULL); }
	|TK_GLOBALLY time_slice sub_f				{ $<node>$= new_node(GLOBALLY, $2, $3); }
	|TK_FINALLY time_slice sub_f				{ $<node>$= new_node(FINALLY, $2, $3); }
	|sub_f TK_UNTIL intermediate_for_until		{ $<node>$= new_node(UNTIL, $1, $3); }
	|'(' f ')'									{ $<node>$= new_node(PARENTESIS, $2, NULL); }
;

intermediate_for_until: time_slice sub_f		{ $<node>$= new_node(INTERMEDIATE, $1, $2); }
;

sub_f: expression
		|sub_f TK_AND sub_f						{ $<node>$= new_node(AND, $1, $3); }
		|sub_f TK_OR sub_f						{ $<node>$= new_node(OR, $1, $3); }
		|TK_NOT sub_f							{ $<node>$= new_node(NOT, $2, NULL); }
		|'(' sub_f ')'							{ $<node>$= new_node(PARENTESIS, $2, NULL); }
;

expression: TK_NUM_DOUBLE						{ $<node>$= new_number($1); }
			|TK_VAR								{ $<node>$= new_variable($1); }
			|expression '+' expression			{ $<node>$= new_node(SUM, $1, $3); }
			|expression '-' expression			{ $<node>$= new_node(SUB, $1, $3); }
			|expression '*' expression			{ $<node>$= new_node(MULT, $1, $3); }
			|expression '/' expression			{ $<node>$= new_node(DIVIDE, $1, $3); }
			|TK_NUM_DOUBLE '^' power			{ $<node>$= new_node(POWER, new_number($1), $3); }
			|'-' expression %prec TK_NEG		{ $<node>$= new_node(NEG, $2, NULL);}
			|expression TK_GE expression		{ $<node>$= new_node(GE, $1, $3); }
			|expression TK_LESS expression		{ $<node>$= new_node(LESS, $1, $3); }
			|'(' expression ')'					{ $<node>$= new_node(PARENTESIS, $2, NULL); }
;

time_slice: '[' TK_NUM_DOUBLE ',' TK_NUM_DOUBLE ']'				{ $<node>$= new_node(TIME, new_number($2), new_number($4)); }
			|'[' '-' TK_NUM_DOUBLE ',' TK_NUM_DOUBLE ']'		{ $<node>$= new_node(TIME, new_number(-$3), new_number($5)); }
			|'[' TK_NUM_DOUBLE ',' '-' TK_NUM_DOUBLE ']'		{ $<node>$= new_node(TIME, new_number($2), new_number(-$5)); }
			|'[' '-' TK_NUM_DOUBLE ',' '-' TK_NUM_DOUBLE ']'	{ $<node>$= new_node(TIME, new_number(-$3), new_number(-$6)); }
;

power: TK_NUM_DOUBLE							{ $<node>$= new_number($1); }
		|'(' power ')'							{ $<node>$= new_node(PARENTESIS, $2, NULL); }
		|power '+' power						{ $<node>$= new_node(SUM, $1, $3); }
		|power '-' power						{ $<node>$= new_node(SUB, $1, $3); }
		|power '/' power						{ $<node>$= new_node(DIVIDE, $1, $3); }
		|power '*' power						{ $<node>$= new_node(MULT, $1, $3); }
		|'-' power %prec TK_NEG					{ $<node>$= new_node(NEG, $2, NULL); }
;  

%%


int main(void)
{	
	yydebug = 0; // 1 for activate debug for yacc/bison
	yyparse(); // start the parsing	
	return 0;
}


