/******************************************************************************
** This file contains the functions for modifing the parse tree.             **
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/

#include"treeUtils.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>


// auxiliary functions
static void convertInCorrectForm(struct node* root); // the form is f >=0
static void simplify2(struct node* root); // auxiliary function
static bool isSimplified(struct node* root); // return true if the tree is simplified


struct node* new_node(enum Operator op, struct node* left_operand, struct node* right_operand)
{
	struct node* r = malloc(sizeof (struct node) );
	r->variable = NULL;
	r->op = op;
	r->left = left_operand;
	r->right = right_operand;
	return r;
}


struct node* new_number(double value)
{
	//printf("num: %f\n", value);
	struct node* r = malloc(sizeof (struct node) );
	r->op=NUM;
	r->value_double=value;
	r->variable = NULL;
	r->left = NULL;
	r->right = NULL;
	return r;
}


struct node* new_variable(char* var)
{
	struct node* r = malloc(sizeof (struct node));
    if (var != NULL)
    {
    	r->variable = malloc(strlen (var) + 1 );
    	strcpy(r->variable, var);
    	//printf("%s\n", r->variable);
    }
    	
	r->op = VAR;
	r->left = NULL; 
	r->right = NULL;
	return r;
}


bool checkTimeSlice(double number1, double number2)
{
	if( ! (number1 >=0 && number2>=0) )
	{
		fprintf(stderr, "Error: incorrect time slice because the time interval is not positive\n");
		return false;
	}

	if( ! ( number2 > number1) )
	{
		fprintf(stderr, "Error: incorrect time slice because empty interval\n");
		return false;
	}
	
	if(! (number1 - floor(number1) == 0 && number2 - floor(number2) == 0) )
	{
		fprintf(stderr, "Error: incorrect time slice because the numbers have to be integer\n");
		return false;
	}
	
	return true;
}


// modify the tree to resolve the NOT in the formulas and convert in f>=0 form
void simplify(struct node* root)
{
	do
	{
		simplify2(root); // some formula require more than 1 traverse for being semplified
		// execute simplify2 until the tree is simplified
	} while(isSimplified(root) == false);
}


// returns void, the root doesn't change memory location(recursively).
void simplify2(struct node* root) // look at LogicParser.y to understand the tree structure
{
	
	if(root == NULL) // base case
		return;
	
	// because of the recursion and the absence of parent pointer in the nodes,
	// the root cannot change memory positio, insetead it's modified!!
	
	// 2 support variables
	struct node* currentNode;
	struct node* temp;
	
	// special cases at the beginning and at the end of the function!
	
	// check the time slice
	if(root->op == TIME)
	{
		if ( checkTimeSlice(root->left->value_double, root->right->value_double) )
			/*empty*/;
		else
			exit(3); // checkTimeSlice print error messages
	}
	
	// special case double parentesis caused by oprations in this function
	if( root->op == PARENTESIS && root->left->op == PARENTESIS )
	{
		temp = root->left;
		root->left = root->left->left;
		free(temp);
	}
	
	// special case (¬(f)) = ¬(f)
	if(root->op == PARENTESIS && root->left->op == NOT && root->left->left->op == PARENTESIS )
	{
		root->op = NOT;
		root->left->op = PARENTESIS;
		temp = root->left->left; // the second parentesis node
		root->left->left = temp->left;
		free(temp);
	}
	
	// the case of unary minus is managed with the NEG node, that rappresent a (-1)*
	// if the child node is a number this semplification in possible
	if( root->op == NEG && root->left->op == NUM)
	{
		root->op = NUM;
		root->value_double = -1* root->left->value_double;
		free(root->left);
		root->left = NULL;
	}
	
	// special case ((f1)∧(f2)) = (f1∧f2) and ((f1)∨(f2)) = (f1∨f2)
	if( root->op == PARENTESIS && (root->left->op == AND || root->left->op == OR) 
		&& root->left->left->op == PARENTESIS && root->left->right->op == PARENTESIS)
	{
		currentNode = root->left; // ∧ node or ∨ node
		temp = currentNode->left; // inner left parentesis
		currentNode->left = currentNode->left->left; // attach f1
		free(temp);
		temp = currentNode->right; // inner right parentesis
		currentNode->right = currentNode->right->left; // attach f2
		free(temp);
	}
	
	// resolve the negation
	if(root->op == NOT)
	{
		
		switch(root->left->op)
		{
			case NOT: // double negation
				currentNode = root->left->left;
				free(root->left); // delete 2° NOT
				// clone the node
				root->op = currentNode->op;
				root->variable = currentNode->variable;
				root->value_double = currentNode->value_double;
				root->left = currentNode->left;
				root->right = currentNode->right;
				
				free(currentNode);
				
				break;

			case GLOBALLY: // ¬G f = F ¬f
				root->op = FINALLY; // modify the NOT node
				temp = root->left;	// the old G node
				root->left = temp->left; // connect time slice
				root->right = new_node(NOT, temp->right, NULL); // connect ¬f
				free(temp);
				break;

			case FINALLY: // ¬F f = G ¬f
				root->op=GLOBALLY; // modify the NOT node
				temp = root->left; // the old F node
				root->left = temp->left; // connect time slice
				root->right = new_node(NOT, temp->right, NULL);
				free(temp);
				break;		
			
			case GE: // >= in the case ¬f
				root->op = LESS;
				temp = root->left;
				root->left = temp->left;
				root->right = temp->right;
				free(temp);
				break;
				
			case LESS: // < in the case ¬f
				root->op = GE;
				temp = root->left;
				root->left = temp->left;
				root->right = temp->right;
				free(temp);
				// the is a special case at the end of the switch because the formula has to be >=
				break;
				
			case PARENTESIS: //¬( )
				
				switch(root->left->left->op)
				{
					case AND: // de Morgan ¬(f∧g) = (¬f∨¬g)
						root->op = PARENTESIS;
						currentNode = root->left->left; // old AND node
						free(root->left); // free old parentesis
						currentNode->op = OR;
						root->left = currentNode; // connect the new () with the new OR
						currentNode->left = new_node(NOT, currentNode->left, NULL);
						currentNode->right = new_node(NOT, currentNode->right, NULL);
						break;
						
					case OR: // de Morgan ¬(f∨g) = (¬f∧¬g)
						root->op = PARENTESIS;
						currentNode = root->left->left; // old OR node
						free(root->left); // free old parentesis
						currentNode->op = AND;
						root->left = currentNode; // connect the new () with the new AND
						currentNode->left = new_node(NOT, currentNode->left, NULL);
						currentNode->right = new_node(NOT, currentNode->right, NULL);		
						break;
				
					case GLOBALLY: // ¬(G f) = (F ¬f) = F ¬(f)						
						root->op = FINALLY;
						currentNode = root->left->left; // old G node
						free(root->left); // old PARENTESIS node
						root->left = currentNode->left; // connect time slice
						root->right = new_node(NOT, new_node(PARENTESIS, currentNode->right, NULL), NULL); // connect ¬(f)
						free(currentNode);
						break;
					
					case FINALLY: // ¬(F f) = (G ¬f) = G ¬(f)
						root->op = FINALLY;
						currentNode = root->left->left; // old F node
						free(root->left); // old PARENTESIS node
						root->left = currentNode->left; // connect time slice
						root->right = new_node(NOT, new_node(PARENTESIS, currentNode->right, NULL), NULL); // connect ¬(f)
						free(currentNode);
						break;
					
					case GE:
						root->op = PARENTESIS;
						currentNode = root->left->left; // GE node
						free(root->left); // old PARENTESIS node
						root->left = currentNode;
						currentNode->op = LESS;
						break;
					
					case LESS:
						root->op = PARENTESIS;
						currentNode = root->left->left; // LESS node
						free(root->left); // old PARENTESIS node
						root->left = currentNode;
						currentNode->op = GE;
						break;
					
					default:
						break;
				} // end 2° switch: the NOT PARENTESIS one
				break;
				
				default:
					break;
		}// end 1° switch: the NOT one
	}// end if for the NOT
	
	// special case for putting the disequation in greater equal form
	// GE and LESS are also modified in the previous switch
	if(root->op == LESS)
	{
		// (-1)*(f)
		root->op = GE;
		root->left = new_node(MULT,
			new_node(PARENTESIS, new_number(-1), NULL),
			new_node(PARENTESIS, root->left, NULL)
		);
		root->right = new_node(MULT,
			new_node(PARENTESIS, new_number(-1), NULL),
			new_node(PARENTESIS, root->right, NULL)
		);	
	}
	
	// special case for putting the formula in f>=0 form
	// require the previous special case
	if(root->op == GE)
		convertInCorrectForm(root);
	
	
	// recursive traverse the tree
	simplify2(root->left);
	simplify2(root->right);
	
}


// modify the tree of the formula to obtain f>=0
// assumption the root is the node GE !
void convertInCorrectForm(struct node* root)
{
	// f1 >= f2 ==> f1-f2 >= 0 ==> f1 + (-1)*(f2) >= 0
	struct node* f2 = root->right;
	struct node* f1 = root->left;
	
	if (root->right->op == NUM && root->right->value_double == 0)
		return;
	
	// build (-1)*(f2)
	f2 = new_node(MULT, new_node(PARENTESIS, new_number(-1), NULL), new_node(PARENTESIS, f2, NULL) );
	root->left = new_node(SUM, f1, f2);
	root->right = new_number(0); // put the 0 on the right	
}


/*
	The tree of STLs is semplified when there aren't negations nor f<0.
	The function returns false if a NOT node or a LESS node is found, otherwise true.
	Simple recursion on the binary tree.
*/
bool isSimplified(struct node* root)
{
	if(root == NULL) // base case
		return true;
	
	if(root->op == NOT || root->op == LESS)
		return false;
		
	// shortcut execution
	if(root->op == GE )
		return true;
	
	// if both the subtree are without NOT and LESS, then return true
	if( isSimplified(root->left) == true && isSimplified(root->right) == true )
		return true;
	else
		return false;
}



