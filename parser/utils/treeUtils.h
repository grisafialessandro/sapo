/******************************************************************************
** Utilities for creating and modifing the parse tree of the logic           **
** formulas(STL). Definition of the structure of the tree. Definition of     **
** the key value of the node: the math operation that node represent. The    **
** enumeration(Operator) is used as key value.                               **
**                                                                           **
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/


#ifndef TREEUTILS_H
#define TREEUTILS_H

#include<stdbool.h> // for checkTimeSlice and internal functions

enum Operator {GLOBALLY, FINALLY, UNTIL, 
				AND, OR, NOT, 
				PARENTESIS, VAR, INTERMEDIATE, NUM, TIME,
				SUM, SUB, MULT, DIVIDE, POWER, NEG,
				GE, LESS};
				
struct node {
	char* variable; // contains the name of the variable
	double value_double; // contains the number even when the time slice require integer
	enum Operator op; // math operation that the node represent
	struct node* left;
	struct node* right;
};

/*>>>> Functions for the creation of new nodes, they return the new node <<<<*/
struct node* new_node(enum Operator op, struct node* left_operand, struct node* right_operand);
// Create the node for the variable, creates a copy of the input string var
struct node* new_variable(char* var);
struct node* new_number(double value);

/*
	This function change the tree to resolve the NOT in the formulas, and put
	the formulas in f>=0 form.
*/
void simplify(struct node* root);

/*
	Make various checks on the time slice (for G, F, U). The time slice MUST HAVE:
	number1>=0, number2>=0, number1 < number2
	number1 and number2 must be integers even they are represented as double.
	Returns true if the previous conditions are met, else false and print in the stderr.
*/
bool checkTimeSlice(double number1, double number2);

#endif


