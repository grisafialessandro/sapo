/******************************************************************************
** This file contains the functions for modifing the parse tree.             **
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/

#include"treePrint.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

// auxiliary functions for recursions
static void print2DUtil(struct node* root, int space);
static void treeToString2(struct node* root, char** results);
static void treeToCode2(struct node* root, char** results);
static char* createString(void);
static void insertString(char** destination, char* input);


// global variable
// used only in printToCode for giving a numeration to the formulas
static int globalCounterForAtoms=0;

// wrapper
char* treeToString(struct node* root)
{
	char* results = createString();
	char** pResults = & results;
	treeToString2(root, pResults);
	return *pResults;
}


void treeToString2(struct node* root, char** results)
{
	if(root == NULL) // base case
		return;
	
	switch (root->op)
	{	
		case GLOBALLY:
			insertString(results, "G_");
			treeToString2(root->left, results);
			treeToString2(root->right, results);
			break;
		
		case FINALLY:
			insertString(results, "F_");
			treeToString2(root->left, results);
			treeToString2(root->right, results);
			break;
			
		case UNTIL:
			treeToString2(root->left, results);
			insertString(results, "_U_");	
			treeToString2(root->right, results);	
			break;
		
			
		case AND:
			treeToString2(root->left, results);
			insertString(results, " ∧ ");
			treeToString2(root->right, results);
			break;
			
		case OR:
			treeToString2(root->left, results);
			insertString(results, " ∨ ");
			treeToString2(root->right, results);
			break;
			
		case NOT:
			insertString(results, "¬");
			treeToString2(root->left, results);
			break;
		
		case TIME:
			{
				char timeNum[100];
				sprintf(timeNum, "[%d,%d] ", (int)root->left->value_double, (int)root->right->value_double);
				insertString(results, timeNum);
			}
			break;
			
		case PARENTESIS:
			insertString(results, "(");
			treeToString2(root->left, results);
			insertString(results, ")");
			break;
			
		case VAR:
			insertString(results, root->variable);
			break;
			
		case INTERMEDIATE:
			treeToString2(root->left, results);
			treeToString2(root->right, results);
			break;
		
		
		case NUM:
			{
				char number[50];
				sprintf(number, "%.6f", root->value_double);
				insertString(results, number);
			}
			break;
		
		case SUM:
			treeToString2(root->left, results);
			insertString(results, "+");
			treeToString2(root->right, results);
			break;
		
		case SUB:
			treeToString2(root->left, results);
			insertString(results, "-");
			treeToString2(root->right, results);
			break;
		
		case MULT:
			treeToString2(root->left, results);
			insertString(results, "*");
			treeToString2(root->right, results);
			break;
		
		case DIVIDE:
			treeToString2(root->left, results);
			insertString(results, "/");
			treeToString2(root->right, results);
			break;
		
		case POWER:
			/*treeToString2(root->left, results);
			insertString(results, "^");
			treeToString2(root->right, results);*/
			insertString(results, "pow(");
			treeToString2(root->left, results);
			insertString(results, ", ");
			treeToString2(root->right, results);
			insertString(results, ")");
			break;
		
		
		case GE:
			treeToString2(root->left, results);
			insertString(results, ">=");
			treeToString2(root->right, results);
			break;
		
		case LESS:
			treeToString2(root->left, results);
			insertString(results, "<");
			treeToString2(root->right, results);
			break;
			
		case NEG: // the single numbers node are modified in semplify
			if(root->left->op == VAR)
				insertString(results, "-"); // print the variable in a more readable format
			else			
				insertString(results, "(-1)*"); // all the other cases
			
			treeToString2(root->left, results);
			break;
	
		default:
			//printf("formula not supported");
			break;
	}
	
}


// wrapper
char* treeToCode(struct node* root)
{
	char* results = createString();
	char** pResults = & results;
	insertString(pResults, "// The logic specification for synthesis\n");
	
	treeToCode2(root, pResults);
	
	insertString(pResults, "this->spec = phi"); // this phi contains all the formulas
	char temp[10];
	sprintf (temp, "%d", globalCounterForAtoms-1); // last phi number used
	insertString(pResults, temp);
	insertString(pResults, ";\n\n");
	
	return *pResults;
}


void treeToCode2(struct node* root, char** results)
{
	if (root == NULL) 
		return;
		
	// counter used with recursion !!! post-order traversal
	int currentCounterForAtoms;  //globalCounterForAtoms; 
	
	char temp[100];
	
	switch(root->op)
	{
		case GE: // ex constraint0 = -q+0.04;
			currentCounterForAtoms = globalCounterForAtoms;
			globalCounterForAtoms++;
			insertString(results, "ex constraint");
			sprintf(temp, "%d", currentCounterForAtoms);
			insertString(results, temp);
			insertString(results, " = ");
			insertString(results, treeToString(root->left));
			insertString(results, ";\n");
			
			//Atom* phi0 = new Atom(constraint0,0);
			insertString(results, "Atom* phi");
			insertString(results, temp);
			insertString(results, " = new Atom(constraint");
			insertString(results, temp);
			insertString(results, ", ");
			insertString(results, temp);
			insertString(results, ");\n\n");
			break;
	
	/*
		phi is the base name for all atoms and formulas.
		It's confusing but simplify the AND, OR case because it's ignored
		if they operate on atoms or formulas.
		It could be changed.
	*/	
		case GLOBALLY: // STL* x = new Always(int a, int b, STL* f);
			
			treeToCode2(root->right, results); // print the atoms before
			
			currentCounterForAtoms = globalCounterForAtoms;
			globalCounterForAtoms++;
			
			insertString(results, "STL* ");
			insertString(results, "phi");
			sprintf(temp, "%d", currentCounterForAtoms);
			insertString(results, temp);
			insertString(results, " = new Always(");
			
			sprintf(temp, "%d", (int) root->left->left->value_double); // start of the time interval
			insertString(results, temp);
			insertString(results, ", ");
			
			sprintf(temp, "%d", (int) root->left->right->value_double); // end of the time interval
			insertString(results, temp);
			insertString(results, ", phi");
			
			sprintf(temp, "%d", currentCounterForAtoms-1); // the phi of the G
			insertString(results, temp);
			insertString(results, ");\n\n");
			break;
			
		case FINALLY: // STL* x= new Eventually(int a, int b, STL* f)
		
			treeToCode2(root->right, results); // print the atoms before
			
			currentCounterForAtoms = globalCounterForAtoms;
			globalCounterForAtoms++;
			
			insertString(results, "STL* ");
			insertString(results, "phi");
			sprintf(temp, "%d", currentCounterForAtoms);
			insertString(results, temp);
			insertString(results, " = new Eventually(");
			
			sprintf(temp, "%d", (int) root->left->left->value_double); // start of the time interval
			insertString(results, temp);
			insertString(results, ", ");
			
			sprintf(temp, "%d", (int) root->left->right->value_double); // end of the time interval
			insertString(results, temp);
			insertString(results, ", phi");
			
			sprintf(temp, "%d", currentCounterForAtoms-1); // the phi of the F
			insertString(results, temp);
			insertString(results, ");\n\n");
			break;
		
		case UNTIL: // STL* x = new Until(STL* f1, int a, int b, STL* f2)
			{
				// the string is formed in partial
				char partial [100];
				sprintf(partial, "%s", "new Until(phi");
				
				treeToCode2(root->left, results); // print the atoms of the first formula
			
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the first phi
				strcat(partial, ", ");
			
				// print start of the time interval, skip intermediate node
				sprintf(temp, "%d", (int) root->right->left->left->value_double);		
				strcat(partial, temp);
				strcat(partial, ", ");
				// print end of the time interval
				sprintf(temp, "%d", (int) root->right->left->right->value_double);		
				strcat(partial, temp);
				strcat(partial, ", ");
			
				// print the atoms of the second formula
				treeToCode2(root->right->right, results); // skip intermediate node
						
				strcat(partial, "phi");
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the second phi
				strcat(partial, ");\n\n");		
			
				currentCounterForAtoms = globalCounterForAtoms;
				globalCounterForAtoms++;
			
				insertString(results, "STL* ");
				insertString(results, "phi");
				sprintf(temp, "%d", currentCounterForAtoms);
				insertString(results, temp);
				insertString(results, " = ");
				insertString(results, partial);
			}
			break;
		
		case AND: // STL* x= new Conjunction(STL* f1, STL* f2); f1, f2 can be Atoms
			{
				// the string is formed in partial
				char partial [100];
				sprintf(partial, "%s", "new Conjunction(phi");
				
				treeToCode2(root->left, results); // print the left (atoms of formulas)
			
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the first phi
				strcat(partial, ", ");
				
				// print the right formula (atoms of formulas)
				treeToCode2(root->right, results);
			
				strcat(partial, "phi");
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the second phi
				strcat(partial, ");\n\n");
				
				currentCounterForAtoms = globalCounterForAtoms;
				globalCounterForAtoms++;
			
				insertString(results, "STL* ");
				insertString(results, "phi");
				sprintf(temp, "%d", currentCounterForAtoms);
				insertString(results, temp);
				insertString(results, " = ");
				insertString(results, partial);
			}
			break;
			
		case OR: // STL* x= new Disjunction(STL* f1, STL* f2); f1, f2 can be Atoms
			{
				// the string is formed in partial
				char partial [100];
				sprintf(partial, "%s", "new Disjunction(phi");
				
				treeToCode2(root->left, results); // print the left (atoms of formulas)
			
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the first phi
				strcat(partial, ", ");
				
				// print the right formula (atoms of formulas)
				treeToCode2(root->right, results);
			
				strcat(partial, "phi");
				sprintf(temp, "%d", globalCounterForAtoms-1);
				strcat(partial, temp); // write number of the second phi
				strcat(partial, ");\n\n");
				
				currentCounterForAtoms = globalCounterForAtoms;
				globalCounterForAtoms++;
			
				insertString(results, "STL* ");
				insertString(results, "phi");
				sprintf(temp, "%d", currentCounterForAtoms);
				insertString(results, temp);
				insertString(results, " = ");
				insertString(results, partial);
			}
			break;
			
		case PARENTESIS:
			// The parentesis in this case are only those between atoms and formulas, not inside atoms.
			treeToCode2(root->left, results);
			break;
			
		default:
			break;
	}
}


// from https://www.geeksforgeeks.org/print-binary-tree-2-dimensions/
// Wrapper over print2DUtil() 
void print2D(struct node* root)
{ 
   // Pass initial space count as 0 
   print2DUtil(root, 0); 
} 

// Function to print binary tree in 2D, it does reverse inorder traversal
void print2DUtil(struct node* root, int space)
{ 
    // Base case 
    if (root == NULL) 
        return; 
	
    // Increase distance between levels 
    space += 10; 

    // Process right child first 
    print2DUtil(root->right, space); 
  
    // Print current node after space 
    // count 
    printf("\n"); 
    for (int i = 10; i < space; i++) 
        printf(" ");
    
    	switch (root->op)
		{
			case GLOBALLY:
				printf("Globally ");
				break;
			case FINALLY:
				printf("Finally ");
				break;
			case UNTIL:
				printf("Until ");
				break;
			case AND:
				printf("AND ");
				break;
			case OR:
				printf("OR ");
				break;
			case NOT:
				printf("NOT ");
				break;
			case TIME:
				printf("TIME ");
				break;
			case PARENTESIS:
				printf("PARENTESIS ");
				break;
			case VAR:
				printf("VAR ");
				break;
			case INTERMEDIATE:
				printf("INTERMEDIATE ");
				break;
			case NUM:
				printf("NUM ");
				break;
			case SUM:
				printf("SUM ");
				break;
			case SUB:
				printf("SUB ");
				break;
			case MULT:
				printf("MULT ");
				break;
			case DIVIDE:
				printf("DIVIDE ");
				break;
			case POWER:
				printf("POWER ");
				break;
			case NEG:
				printf("NEG ");
				break;
			case GE:
				printf("GE ");
				break;
			case LESS:
				printf("LESS ");
				break;

			
			default:
				break;
		}
		
	if(root->op == NUM)
		printf("%f ", root->value_double);

    if(root->variable != NULL)
    	printf("%s ", root->variable);
  
    // Process left child 
    print2DUtil(root->left, space); 
}


// functions for strings
char* createString(void)
{
	char* string = malloc( 100 * sizeof (char));
	if(string == NULL)
	{
		fprintf(stderr, "Error: Failed string creation\n");
		fflush(stderr);
		exit(1); // terminate
	}
	string[0]='\0';
	return string;
}


// find a library !!!
void insertString(char** destination, char* input)
{
	/*printf("Inserted string:\n");
	printf("dest: %s\n", destination);
	printf("input: %s\n", input);*/

	*destination = realloc(*destination, ( strlen(input) + strlen(*destination) +1 ) * sizeof(char));
	
	if(*destination == NULL)
	{
		fprintf(stderr, "Error: Failed string resizing\n");
		fflush(stderr);
		exit(2); // terminate
	}
	
	strcat(*destination, input);
}
