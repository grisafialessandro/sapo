/******************************************************************************
** Utilities for printing the parse tree of the logic formulas(STL) in       **
** various form.                                                             **
**                                                                           **
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/

#ifndef TREEPRINT_H
#define TREEPRINT_H

#include"treeUtils.h" // structure and enumeration

// Print the tree in 2D, from https://www.geeksforgeeks.org/print-binary-tree-2-dimensions/
void print2D(struct node* root);
// Return the STL: traverse the tree and return the string of the formula.
// Numbers are printed with 6 decimals. Power is printed like pow(n,m).
char* treeToString(struct node* root);
// Return the string containing the C++ code to put in sapo.
// Numbers are printed with 6 decimals. Possible problem see readme.txt.
// Power is printed like pow(n,m).
char* treeToCode(struct node* root);

#endif
