/******************************************************************************
** This file contains the lexical definition for the formulas (modified STL) **
**                                                                           **
** @author Gianluca Ermacora <ermacora.gianluca25@gmail.com>                 **
******************************************************************************/

%{
	#include"LogicParser.tab.h"
	#include<stdlib.h> // atof
%}

digits		[0-9]+
integer		{digits}
rational	{digits}"."{digits}
number		{rational}|{integer}
globally	G_
finally		F_
until		_U_

%%

{number}	{yylval.fp=atof(yytext); return TK_NUM_DOUBLE;} // converts in double also the int of the time slice of G, F, U.
"∧"			{return TK_AND;}
"∨"			{return TK_OR;}
"¬"			{return TK_NOT;}

{globally}	{return TK_GLOBALLY;}
{finally}	{return TK_FINALLY;}
{until}		{return TK_UNTIL;}

[a-zA-Z]+	{yylval.str=yytext; return TK_VAR;} // There is the assumption that variable are only made of characters (parameters are not allowed)
">="		{return TK_GE;}
"<"			{return TK_LESS;}
[ \t]+		{} // ignore space and tab
\n			{return TK_END;}
.           {return yytext[0];} // accepts all single char token like: +-*/()

%%

void yyerror() 
{ 
	fprintf(stderr, "Error: incorrect parsing\n");
	exit(4);
}

int yywrap()
{
	//aquire only one input line
	return 1;
}

